import urllib.request
import json
import time

def get_data():
    url = "http://coffeeman.taistelumarsu.org/NP-Fi-Parser/api.php?callback2"
    uh = urllib.request.urlopen(url)
    data = uh.read()
    js = data.decode("utf-8")
    js = js.replace("(", "")
    js = js.replace(")", "")

    return json.loads(js)

# Amount of minimum prices we are interested in
num_prices = 4

min_prices = []
for counter in range(0,num_prices):
    min_prices.append([999,0])

def is_cheap(price, time):
    c = num_prices - 1
    if price < min_prices[c][0]:
        min_prices[c] = [price, time]
        min_prices.sort()

json_data = get_data()

count = 24
for element in reversed(json_data):
    count = count - 1
    is_cheap(element[1], element[0])
    if count == 0:
        break

for val in min_prices:
    print(val)